if (!require('fs').existsSync('pe-scripts')) {
    const packageJson = require(`${__dirname}/../package.json`);
    const version = packageJson['protopia-ecosystem'].scripts;
    require('child_process').execSync(
        `git clone https://gitlab.com/dilesoft-projects/pe-react-scripts --branch ${version} pe-scripts`,
        {stdio: 'inherit'}
    );
}
require('child_process').execSync(
    'node pe-scripts/install.js',
    {stdio: 'inherit'}
);